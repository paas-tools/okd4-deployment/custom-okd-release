# custom-okd-release

This project builds and stores our customized OKD releases.
They are customized because we inject (overwrite) a set of container images with our own forks.

## Generate a new OKD release

The full procedure for generating a new custom OKD release is described in the [okd-internal documentation](https://okd-internal.docs.cern.ch/operations/upgrade-cluster/), but here's the gist of it:

First, to modify the value of `UPSTREAM_OKD_TAG` in case needed. To find the latest OKD releases go to <https://github.com/openshift/okd/releases>.

```yaml
variables:
  UPSTREAM_OKD_TAG: "4.7.0-0.okd-2021-02-25-144700"
```

Update each of the customized and components and add the digest of the newly built images, e.g..:

```yaml
...
# Custom images
  CUSTOM_HAPROXY_ROUTER_IMAGE: "gitlab-registry.cern.ch/paas-tools/okd4-deployment/custom-ingress:<desired_value>"
...
```

And that's all. Once committed, create a merge request in this repo, which will trigger a job that generates a new image in the form `registry.cern.ch/paas-tools/okd4-deployment/custom-okd-release:${UPSTREAM_OKD_TAG}-cern-${CI_COMMIT_SHORT_SHA}`.

Once this commit hits the master branch of this repo, the job will also mirror the okd-contents images to Gitlab registry.

Use `oc adm release info "${CUSTOM_OKD_IMAGE}"` to show detailed information about this release (version, digest, kubernetes version etc.)

### Add new custom cluster operators

When we need to add more custom cluster operators, add them to the `variables` part in the `.gitlab-ci.yml` file:

```yaml
...
# Custom images
  CUSTOM_HAPROXY_ROUTER_IMAGE: "gitlab-registry.cern.ch/paas-tools/okd4-deployment/custom-ingress:<desired_value>"
  CUSTOM_MY_CLUSTER_OPERATOR_IMAGE: "gitlab-registry.cern.ch/.../my-custom-cluster-operator:<desired_value>"
...
```

Finally, append its reference at the end of the `oc adm release new` command:

```bash
oc adm release new --from-release="${UPSTREAM_OKD_VERSION}" \
      --to-image="${RELEASE_OKD_VERSION}" \
      haproxy-router="${CUSTOM_HAPROXY_ROUTER_IMAGE}" \
      my-custom-cluster-operator="${CUSTOM_MY_CLUSTER_OPERATOR_IMAGE}"
```
